<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property mixed dbforge
 * @property mixed db
 * @property mixed config
 * @property mixed load
 */
class Migration_support extends CI_Migration {
    
    public function __construct() {}

    public function up() {        

        //Таблица обращений от пользователей        
        $this->dbforge->add_field(array(
            'id' => array('type' => 'BIGINT', 'unsigned' => TRUE, 'auto_increment' => TRUE),
            'name' => array('type' => 'VARCHAR', 'constraint' => '255'),
            'email' => array('type' => 'VARCHAR', 'constraint' => '255'),
            'theme_id' => array('type' => 'TINYINT', 'constraint' => '3', 'unsigned' => TRUE),
            '`date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
            'message' => array('type' => 'TEXT'),            
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('support_messages');

        // Таблица тем обращений от пользователей        
        $this->dbforge->add_field(array(            
            'theme_id' => array('type' => 'TINYINT', 'constraint' => '3', 'unsigned' => TRUE, 'auto_increment' => TRUE),
            'theme' => array('type' => 'VARCHAR', 'constraint' => '255'),
            'active' => array('type' => 'ENUM("0","1")', 'default' => '1'),            
        ));
        $this->dbforge->add_key('theme_id', TRUE);
        $this->dbforge->create_table('support_themes');

        // Пишем в таблицу тем обращений от пользователей
        $data = array(
            array('theme' => 'Общий вопрос'),
            array('theme' => 'Жалоба на пользователя/группу'),            
        );
        $this->db->insert_batch('support_themes', $data); 
    }

    public function down() {
        log_message('info', 'Инициализирующую миграцию откатить нельзя');
        show_error('Инициализирующую миграцию откатить нельзя.');
    }
}