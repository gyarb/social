<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('support_model');
    $this->config->load('recaptcha');   
  }

  public function index() {        
    $data['h'] = 'Поддержка';
    $data['content'] = 'Напишите нам, если есть вопросы.';
    $data['themes'] = $this->support_model->get_themes('active');
    $data['key'] = $this->config->item('recaptcha_site_key');
    $data['result'] = '';

    if ($this->form_validation->run('support') !== FALSE)
    {
      $this->support_model->insert_message();
      redirect('support');
    }
    if($this->session->userdata('message_result') !== null) {
      $data['result'] = $this->session->userdata('message_result');      
      $this->session->unset_userdata('message_result');      
    }
    if($this->ion_auth->logged_in()) {
      $user = $this->ion_auth->user()->row();
      $data['email'] = $user->email;
      $data['name'] = $user->first_name . ' ' . $user->last_name;
    } else {
      $data['email'] = '';
      $data['name'] = '';
    }
    if(set_value('name')) {
      $data['name'] = set_value('name');
    }
    if(set_value('email')) {
      $data['email'] = set_value('email');
    }

    $this->theme
    ->title('Поддержка')
    ->keywords('поддержка,rusimperia')
    ->description('Служба поддержки RusImperia')
    ->add_partial('header')
    ->add_partial('l_sidebar')
    ->add_partial('r_sidebar')
    ->add_partial('footer')
    ->load('common/support', $data);
  }

  function validate_captcha() {    
    $secret_key = $this->config->item('recaptcha_secret_key');
    $captcha = $this->input->post('g-recaptcha-response');
    $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $secret_key . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']));
    if ($response->success == false) {        
        $this->form_validation->set_message('validate_captcha','Если Вы не робот, то поставьте галочку!');       
        return FALSE;
    } else {              
        return TRUE;
    }
  }
  
}