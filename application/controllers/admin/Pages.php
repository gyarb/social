<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

  private $data_admin;

  public function __construct() {
    parent::__construct();
    if(!$this->ion_auth->logged_in() or !$this->ion_auth->is_admin())
    {
      redirect('auth/login');
    } else {
      $this->load->model('page_model');
      $this->load->model('admindata_model');
      $this->data_admin = $this->admindata_model->get_data();
    }
  }

  public function index() {
    redirect('admin/pages/all');
  }

  public function all() {
    $data['title'] = 'Страницы';
    $data['admin'] = $this->ion_auth->user()->row();        
    $data['pages'] = $this->page_model->get_pages();
    $this->load->view('admin/header', $this->data_admin);
    $this->load->view('admin/pages', $data);
    $this->load->view('admin/footer');
  }

  public function add() {
    $data['title'] = 'Добавление страницы';
    $data['admin'] = $this->ion_auth->user()->row();
    $data['result'] = '';

    if($this->session->userdata('page_add_result') !== null) {
      $data['result'] = $this->session->userdata('page_add_result');
      $data['id'] = $this->session->userdata('page_add_id');
      $this->session->unset_userdata('page_add_result');
    }         

    $this->load->view('admin/header', $this->data_admin);
    $this->load->view('admin/pages_add', $data);
    $this->load->view('admin/footer');
  }

  public function item($page_id) {
    $data['title'] = 'Редактирование страницы';
    $data['result'] = '';

    if($this->session->userdata('page_edit_result') !== null) {
      $data['result'] = $this->session->userdata('page_edit_result');
      $this->session->unset_userdata('page_edit_result');
      if($data['result'] == 'ok' || $data['result'] == 'error') {
        $data['page'] = $this->page_model->get_page((int)$page_id);
      }
    } else {
      $data['page'] = $this->page_model->get_page((int)$page_id);
      if(!$data['page']) {
        redirect('admin/pages/all');
      }
    }
    
    $this->load->view('admin/header', $this->data_admin);
    $this->load->view('admin/pages_item', $data);
    $this->load->view('admin/footer');
  }

  public function insert_page() {
    $this->page_model->insert_page();
    redirect('admin/pages/add');
  }

  public function edit_page() {
    $id = $this->input->post('page_id');
    $this->page_model->edit_page($id);
    redirect('admin/pages/item/'.$id);
  }
}
