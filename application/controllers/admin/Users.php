<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

  private $data_admin;

  public function __construct() {
    parent::__construct();
    if(!$this->ion_auth->logged_in() or !$this->ion_auth->is_admin())
    {
      redirect('auth/login');
    } else {
      $this->load->model('user_model');
      $this->load->model('admindata_model');
      $this->data_admin = $this->admindata_model->get_data();
    }
  }

  public function index() {
    redirect('admin/users/all');
  }

  public function all() {
    $data['title'] = 'Пользователи';

    $this->config->load('pagination_admin');
    $config = $this->config->item('admin');    
    $config['base_url'] = base_url().'admin/users/all/';
    $config['total_rows'] = $this->user_model->get_count();    
    $this->pagination->initialize($config);
    $data['users'] = $this->user_model->get_users($config['per_page'], $this->uri->segment(4));

    $this->load->view('admin/header', $this->data_admin);
    $this->load->view('admin/users', $data);
    $this->load->view('admin/footer');
  }

  public function add() {

    $data['title'] = 'Добавление пользователя';    
    $data['id'] = '';       

    $this->load->view('admin/header', $this->data_admin);
    $this->load->view('admin/users_add', $data);
    $this->load->view('admin/footer');
  }

  public function item($user_id) {
    $data['title'] = 'Редактирование пользователя';    
    $data['user'] = $this->ion_auth->user($user_id)->row();

    $this->load->model('media_model');
    $data_avatar = $this->media_model->get_media($user_id);
    if($data_avatar) {
      $data['avatar'] = $data_avatar->file_hash . '.' . $data_avatar->file_ext;
    } else {
      $data['avatar'] = 'nophoto.jpg';
    }    

    $data['result'] = '';
    if($this->session->userdata('user_edit_result') !== null) {
      $data['result'] = $this->session->userdata('user_edit_result');
      $this->session->unset_userdata('user_edit_result');
    }   

    $this->load->view('admin/header', $this->data_admin);
    $this->load->view('admin/users_item', $data);
    $this->load->view('admin/footer');
  }

  public function add_user() {
    
  }

  public function edit_user() {
    $id = $this->input->post('user_id');
    $this->user_model->edit_user($id);
    redirect('admin/users/item/'.$id);
  }
}
