<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends CI_Controller {

  private $data_admin;

  public function __construct() {
    parent::__construct();
    if(!$this->ion_auth->logged_in() or !$this->ion_auth->is_admin())
    {
      redirect('auth/login');
    } else {
      $this->load->model('support_model');
      $this->load->model('admindata_model');
      $this->data_admin = $this->admindata_model->get_data();
    }
  }

  public function index() {
    redirect('admin/support/all');
  }

  public function all() {
    $data['title'] = 'Поддержка';

    $this->config->load('pagination_admin');
    $config = $this->config->item('admin');
    $config['base_url'] = base_url().'admin/support/all/';
    $config['total_rows'] = $this->support_model->get_count();    
    $this->pagination->initialize($config);
    $data['messages'] = $this->support_model->get_messages($config['per_page'], $this->uri->segment(4));

    $this->load->view('admin/header',$this->data_admin);
    $this->load->view('admin/support',$data);
    $this->load->view('admin/footer');
  }  

  public function item($id) {
    $data['title'] = 'Сообщение от пользователя';
    $data['message'] = '';    
    $data['result'] = '';
    if(isset($_SESSION['delete_message'])) {
      $data['result'] = $_SESSION['delete_message'];
      unset($_SESSION['delete_message']);
    } else {
      $data['message'] = $this->support_model->get_message($id);
      if(!$data['message']) {
        redirect('admin/support/all');
      }
    }

    $this->load->view('admin/header', $this->data_admin);
    $this->load->view('admin/support_item', $data);
    $this->load->view('admin/footer');
  }

  public function delete_message() {
    $id = $this->input->post('id');
    $this->support_model->delete_message($id);
    redirect('admin/support/item/'.$id);
  }

  public function themes() {
    $data['title'] = 'Темы сообщений';
    $data['themes'] = $this->support_model->get_themes();
    $data['result'] = '';

    if ($this->form_validation->run() !== FALSE) {
      $this->support_model->add_theme();
      redirect('admin/support/themes');
    }

    if(isset($_SESSION['add_theme'])) {
      $data['result'] = $_SESSION['add_theme'];
      unset($_SESSION['add_theme']);
    }

    $this->load->view('admin/header',$this->data_admin);
    $this->load->view('admin/support_themes',$data);
    $this->load->view('admin/footer');
  }

  public function themes_item($id) {
    $data['title'] = 'Редактирование темы сообщений';        
    $data['result'] = '';

    if ($this->form_validation->run('admin/support/themes_item') !== FALSE) {
      $this->support_model->edit_theme($id);
      redirect('admin/support/themes_item/'.$id);
    }

    if(isset($_SESSION['edit_theme'])) {
      $data['result'] = $_SESSION['edit_theme'];
      unset($_SESSION['edit_theme']);
      if($data['result'] == 'ok' || $data['result'] == 'error' || $data['result'] == 'delete_no') {
        $data['theme'] = $this->support_model->get_theme($id);
      }
    } else {
      $data['theme'] = $this->support_model->get_theme($id);
      if(!$data['theme']) {
        redirect('admin/support/all');
      }
    }

    $this->load->view('admin/header',$this->data_admin);
    $this->load->view('admin/support_themes_item',$data);
    $this->load->view('admin/footer');
  }

  // public function edit_theme() {
  //   $theme_id = $this->input->post('theme_id');
  //   $this->support_model->edit_theme($theme_id);
  //   redirect('admin/support/themes_item/'.$theme_id);
  // }

}
