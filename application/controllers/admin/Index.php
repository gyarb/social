<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

    private $data_admin;

    public function __construct() {
        parent::__construct();
        if(!$this->ion_auth->logged_in() or !$this->ion_auth->is_admin())
        {
            redirect('auth/login');
        } else {
            $this->load->model('admindata_model');
            $this->data_admin = $this->admindata_model->get_data();
        }
    }

 //    public function index() {
 //        $data['title'] = 'Админка';
 //        $data['active'] = 'index';
 //        $this->load->view('admin/header',$data);
	// 	$this->load->view('admin/index');
 //        $this->load->view('admin/footer');
	// }

    public function index() {        
        $data['title'] = 'Главная';        
        $this->load->view('admin/header', $this->data_admin);
        $this->load->view('admin/index', $data);
        $this->load->view('admin/footer');
    }
}
