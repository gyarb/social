<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Photo extends CI_Controller
{
    public $user;
    public $type;
    public $owner_id;

    public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            redirect('', 'refresh');
        } else {
            $this->user = $this->ion_auth->user()->row();
        }
        $this->load->model('photo_model');
        $this->lang->load('photo');
    }

    public function init($type, $id = null) {
        $this->type = $type;
        $this->owner_id = $id;
        $this->photo_model->init($this->type, $this->owner_id);
    }

    public function index($type = false, $id = null) {
        $type = ($type)?$type:'user';
        $owner_id = (empty($id))?$this->user->id:$id;
        $this->init($type, $owner_id);
        $data['type'] = $type;
        $data['id'] = $owner_id;
        $data['albums'] = $this->photo_model->get_albums();
        $data['countAlbums'] = $this->photo_model->get_count_albums();
        $data['photos'] = $this->photo_model->get_ll_photo();
        $data['countPhotos'] = $this->photo_model->get_count_photos();

        $debug = array();
        if (DEBUG) {
            $debug['debug'][] = array(
                't' => 'Информация о пользователе',
                'c' => pretty_print($data)
            );
        }
        $this->theme
            ->title('Фото')
            ->add_partial('header')
            ->add_partial('l_sidebar')
            ->add_partial('r_sidebar')
            ->add_partial('footer', $debug)
            ->load('photo/list', $data);
    }

    public function album($type = false, $owner_id = false, $album_id = null) {
        if ((int)$album_id != 0) {
            $type = ($type)?$type:'user';
            $id = ($owner_id)?$owner_id:$this->user->id;
            $this->init($type, $id);
            $data['owner'] = $type;
            $data['owner_id'] = $owner_id;
            $data['album'] = $this->photo_model->get_album_info($album_id);
            $data['photos'] = $this->photo_model->get_album((int)$album_id);
            $debug = array();
            if (DEBUG) {
                $debug['debug'][] = array(
                    't' => 'Информация',
                    'c' => pretty_print($data)
                );
            }
            $this->theme
                ->title('Фото')
                ->add_partial('header')
                ->add_partial('l_sidebar')
                ->add_partial('r_sidebar')
                ->add_partial('footer', $debug)
                ->load('photo/album', $data);
        } else {
            show_404();
        }
    }

    public function get_modal() {
        $type = $this->input->post('type');
        $html['h'] = $this->lang->line('modal_title_'.$type);
        $html['b'] = $this->theme->view('photo/modal_'.$type, array(), true);
        $ret['status'] = "OK";
        $ret['html'] = $html;
        echo json_encode($ret);
    }



    public function create_user_album() {
        $this->init('user');
        $this->load->library('form_validation');
        if ($this->form_validation->run('createAlbum') === FALSE) {
            $ret['status'] = "ERR";
            $ret['name_err'] = $this->form_validation->error('name');
            $ret['descr_err'] = $this->form_validation->error('descr');
            $ret['message'] = 'Что-то пошло не так, попробуйте позже';
            if (DEBUG) $ret['error'] = 'не прошла валидация';
        } else {
            if ($id = is_numeric($this->photo_model->create_album(
                $this->input->post('name'),
                $this->input->post('descr'),
                $this->user->id
            ))) {
                $ret['status'] = "OK";
                $ret['message'] = 'Альбом успешно создан';
                $ret['album_id'] = $id;
            } else {
                $ret['status'] = "ERR";
                $ret['message'] = 'Что-то пошло не так, попробуйте позже';
                if (DEBUG) $ret['error'] = 'ошибка записи в БД';
            }
        }
        echo json_encode($ret);
    }

    public function get_albums() {
        $type = $this->input->post('type');
        $id = $this->input->post('id');
        $this->init($type, $id);
        $albums = $this->photo_model->get_albums($id, array(), $limit = 10000, $offset = 3);
        $ret['status'] = "OK";
        $albumsH = array();
        foreach ($albums as $a) {
            $albumsH[] = '<div class="album">
                <img class="album-cover" src="/media/photo/cover/'.$a->cover.'" />
                <a class="album-title" href="/photo/album/'.$a->id.'">'.$a->name.'</a>
                <p class="album-description">'.$a->description.'</p>
            </div>';
        }
        $ret['albums'] =$albumsH;
        echo json_encode($ret);
    }

    public function add_photo() {
        $this->photo_model->add_photo();
        //redirect('photo');
    }

    public function add_avatar() {
        /*$album_id = $this->photo_model->get_profile_album($this->user->id);
        $new_avatar = $this->photo_model->add_photo($album_id);
        $this->photo_model->update_avatar($new_avatar);*/
    }

    public function get_lazy() {
        $ret = false;
        echo json_encode($ret);
    }
}
