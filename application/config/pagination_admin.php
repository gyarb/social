<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['admin']['per_page'] = 15; //количество записей на странице
$config['admin']['num_links'] = 2;
$config['admin']['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
$config['admin']['full_tag_close'] = '</ul>';
$config['admin']['first_link'] = FALSE;
$config['admin']['last_link'] = FALSE;
$config['admin']['prev_link'] = '&laquo;';
$config['admin']['next_link'] = '&raquo;';
$config['admin']['prev_tag_open'] = '<li>';
$config['admin']['prev_tag_close'] = '</li>';
$config['admin']['next_tag_open'] = '<li>';
$config['admin']['next_tag_close'] = '</li>';
$config['admin']['num_tag_open'] = '<li>';
$config['admin']['num_tag_close'] = '</li>';
$config['admin']['cur_tag_open'] = '<li><a><b>';
$config['admin']['cur_tag_close'] = '</b></a></li>';