<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['modal_title_user_album'] = 'Создать альбом';
$lang['modal_label_album_title'] = 'Заголовок альбома';
$lang['modal_label_album_descr'] = 'Описание альбома';
$lang['modal_label_album_save'] = 'Сохранить альбом';

$lang['modal_title_user_photo'] = 'Загрузить фотографию';
$lang['modal_label_photo_album_select'] = 'Выберите альбом';
$lang['modal_label_photo_select'] = 'Выберите фото';
$lang['modal_label_photo_descr'] = 'Описание фото';
$lang['modal_label_photo_save'] = 'Загрузить';