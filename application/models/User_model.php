<?php

class User_model extends CI_Model
{
  private $user_table = 'user';
  private $user_group_table = 'user_group';

  public function __construct() { }

  public function get_count() {
    $data = $this->db->count_all_results($this->user_table);
    return $data;
  }

  public function get_users($num, $offset) {
    $data = $this->db->get($this->user_table, $num, $offset)->result();
    return $data;
  }

  public function edit_user($id) {
    $active_old = $this->input->post('active_old');
    $first_name = $this->input->post('first_name',true);
    $first_name = htmlspecialchars(strip_tags(stripslashes($first_name)));
    $last_name = $this->input->post('last_name',true);
    $last_name = htmlspecialchars(strip_tags(stripslashes($last_name)));
    $data_user = array(
      'first_name' => $first_name,
      'last_name' => $last_name,
      'gender' => $this->input->post('gender'),
      'active' => $this->input->post('active'),
    );
    //из активного (user.active=1) в заблокированного (user.active=0)
    if($data_user['active'] == 0 && $active_old == 1) { 
      $data_group = array('group_id' => 2); //group: blocked (group.id=2)
    }
    //из заблокированного (active=0) в активного (active=1)
    if($data_user['active'] == 1 && $active_old == 0) {
      $data_group = array('group_id' => 10); //group: newbie (group.id=10)
    }

    $res_user_group = true;
    if($data_user['active'] != $active_old) {      
      $res_user_group = $this->db->update($this->user_group_table, $data_group, 'user_id ='.$id);      
    }

    $res_user = $this->db->update($this->user_table, $data_user, 'id ='.$id);

    if($res_user && $res_user_group) {  
      $res = 1;
    } else {
      $res = 0;
    }

    if($res) {
      $this->session->set_userdata(array('user_edit_result' => 'ok'));            
    } else {
      $this->session->set_userdata(array('user_edit_result' => 'error'));
    }
  }

}