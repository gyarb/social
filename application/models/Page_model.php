<?php

class Page_model extends CI_Model
{
  private $page_table = 'static_page';

  public function __construct() { }

  public function get_page($page_id) {
    if (is_int($page_id)) {
      $this->db->where('id', $page_id, true);
    } elseif (is_string($page_id)) {
      $this->db->where('name', $page_id, true);
    } else {
      return false;
    }
    return $this->db->from($this->page_table)
    ->limit(1)
    ->get()->row();
  }

  public function  get_pages() {
    return $this->db->get($this->page_table)->result();
  }

  private function clear_text($text) {
    return htmlspecialchars(strip_tags(stripslashes(trim($text))));
  }

  public function insert_page() {
    $title = $this->clear_text($this->input->post('title',true));
    $name = $this->clear_text(strtolower($this->input->post('name',true)));
    $descr = $this->clear_text($this->input->post('descr',true));
    $keywords = $this->clear_text($this->input->post('keywords',true));
    $head = $this->input->post('head',true);
    $content = $this->input->post('content',true);
    $data = array(
      'title' => $title,
      'name' => $name,
      'descr' => $descr,
      'keywords' => $keywords,
      'head' => $head,
      'content' => $content,        
    );
    $res = $this->db->insert($this->page_table, $data);
    if($res) {
      $this->session->set_userdata(array(
        'page_add_result' => 'ok',
        'page_add_id' => $this->db->insert_id(),
      ));           
    } else {
      $this->session->set_userdata(array(
        'page_add_result' => 'error',
        'page_add_id' => '',
      ));        
    }
  }
  
  public function edit_page($id) {
    if(isset($_POST['edit_page'])) {
      $title = $this->clear_text($this->input->post('title',true));
      $name = $this->clear_text(strtolower($this->input->post('name',true)));
      $descr = $this->clear_text($this->input->post('descr',true));
      $keywords = $this->clear_text($this->input->post('keywords',true));
      $head = $this->input->post('head',true);
      $content = $this->input->post('content',true);
      $data = array(
        'title' => $title,
        'name' => $name,
        'descr' => $descr,
        'keywords' => $keywords,
        'head' => $head,
        'content' => $content,        
      );
      $res = $this->db->update($this->page_table, $data, 'id ='.$id);
      if($res) {
        $this->session->set_userdata(array('page_edit_result' => 'ok'));            
      } else {
        $this->session->set_userdata(array('page_edit_result' => 'error'));
      }      
    }
    if(isset($_POST['delete_page'])) {
      $res = $this->db->delete($this->page_table, array('id' => $id));
      if($res) {
        $this->session->set_userdata(array('page_edit_result' => 'delete_ok'));            
      } else {
        $this->session->set_userdata(array('page_edit_result' => 'delete_error'));
      }
    }
    
  }

}