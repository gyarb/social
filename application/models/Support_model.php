<?php

class Support_model extends CI_Model
{
	public function __construct() {}

  public function get_count() {
    $data = $this->db->count_all_results('support_messages');
    return $data;
  }

	public function get_messages($num, $offset) {
    if(!$offset) {
      $offset = 0;
    }
		$data = $this->db->query(
      'SELECT support_messages.*, support_themes.theme 
      FROM support_messages
      JOIN support_themes
      USING (theme_id) 
      LIMIT '.$offset.', '.$num
    )->result();		
		return $data;
	}

	public function get_message($id) {
		$data = $this->db->query(
      'SELECT support_messages.*, support_themes.theme 
      FROM support_messages
      JOIN support_themes
      USING (theme_id)
      WHERE  support_messages.id='.$id
    )->row();		
		return $data;
	}

  public function insert_message() {
    $message['name'] = $this->input->post('name');
    $message['email'] = $this->input->post('email');
    $message['theme_id'] = $this->input->post('theme');
    $message['message'] = $this->input->post('message');
    $res = $this->db->insert('support_messages', $message);
    if($res) {
      $this->session->set_userdata(array('message_result' => 'ok'));            
    } else {
      $this->session->set_userdata(array('message_result' => 'error'));
    }    
  }

  public function delete_message($id) {
    $res = $this->db->delete('support_messages', array('id' => $id));
    if($res) {          
      $_SESSION['delete_message'] = 'ok';
    } else {
      $_SESSION['delete_message'] = 'error';
    }
  }

  public function get_themes($active = '') {    
    if($active == 'active') {
      $this->db->where('active', '1');
    }
    $data = $this->db->get('support_themes')->result();       
    return $data;
  }

  public function get_theme($id) {
    $data = $this->db->where(array('theme_id' => $id));
    $data = $this->db->get('support_themes')->row();   
    return $data;
  }

  public function add_theme() {
    if(isset($_POST['add_theme'])) {
      $theme = trim($this->input->post('theme',true));
      if(!empty($theme)) {        
        $res = $this->db->insert('support_themes', array('theme' => $theme));
        if($res) {
          $_SESSION['add_theme'] = 'ok';
        } else {
          $_SESSION['add_theme'] = 'error';
        }
      }
    }
  }

  public function edit_theme($theme_id) {
    if(isset($_POST['edit_theme'])) {
      $theme = trim($this->input->post('theme',true));
      if(!empty($theme)) {        
        $active = $this->input->post('active');
        $this->db->where('theme_id', $theme_id);
        $res = $this->db->update('support_themes', array('theme' => $theme, 'active' => $active));
        if($res) {          
          $_SESSION['edit_theme'] = 'ok';
        } else {
          $_SESSION['edit_theme'] = 'error';
        }
      }
    }
    if(isset($_POST['delete_theme'])) {
      $this->db->where(array('theme_id' => $theme_id));
      $count_messages = $this->db->count_all_results('support_messages');
      if($count_messages == 0) {
        $res = $this->db->delete('support_themes', array('theme_id' => $theme_id));
        if($res) {          
          $_SESSION['edit_theme'] = 'delete_ok';
        } else {
          $_SESSION['edit_theme'] = 'delete_error';
        }
      } else {
        $_SESSION['edit_theme'] = 'delete_no';
      }
      
    }
  }

}