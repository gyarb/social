<?php

class Admindata_model extends CI_Model
{
	public function __construct() {
    
  }

  public function get_data() {
    $data['support_count'] = $this->db->count_all_results('support_messages');
    $data['admin'] = $this->ion_auth->user()->row();
    return $data;
  }
  
}