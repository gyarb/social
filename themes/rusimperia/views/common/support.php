<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="content-box">
  <h1 class="page-title"><?=$h?></h1>
  <div class="page-content"><?=$content?><br><br></div>

  <?php if($result != ''): ?>

    <?php if($result == 'ok'): ?>
      <p>Ваше сообщение успешно отправлено!</p>
    <?php endif; ?>
    <?php if($result == 'error'): ?>
      <p>При отправлении сообщения поизошла ошибка!</p>
    <?php endif; ?>

  <?php else: ?>  
  
    <!-- <?php echo form_open('support'); ?> -->
    <form class="form form-inline" method="POST" action="/support">
    
    <label class="form-label" for="name">Ваше имя</label>
    <div class="form-group">
      <input type="text" name="name" id="name" class="form-control" value="<?= $name; ?>" placeholder="Ваше Имя" required>        
      <span class="text-danger"><?php echo form_error('name'); ?></span>
    </div>
    
    <label class="form-label" for="email">Ваш email</label>
    <div class="form-group">      
      <input type="email" name="email" id="email" class="form-control" value="<?= $email; ?>" placeholder="Ваш Email" required>
      <span class="text-danger"><?php echo form_error('email'); ?></span>
    </div>

    <label for="theme" class="form-label">Тема сообщения</label>
    <div class="form-group">
      <select name="theme" id="theme" class="form-control">
        <?php foreach($themes as $item): ?>
          <option value="<?= $item->theme_id; ?>" <?= set_select('theme', $item->theme_id); ?>><?= $item->theme; ?></option>
        <?php endforeach; ?>
      </select>
    </div>

    <label for="message" class="form-label">Сообщение</label>
    <div class="form-group">
      <textarea name="message" id="message" class="form-control" required><?= set_value('message'); ?></textarea>
      <?php echo form_error('message'); ?>
    </div>

    <label class="form-label">Вы человек?</label>
    <div class="form-group">
      <div class="g-recaptcha" data-sitekey="<?= $key; ?>"></div>
      <span class="text-danger"><?php echo form_error('g-recaptcha-response'); ?></span>
    </div>

    <div class="form-wide form-right">
      <button type="submit" name="user_message" class="btn btn-success">Отправить сообщение</button>
    </div>   
  </form>

  <?php endif; ?>

</div>
<div class="social-wrap">
  <div class="content-box social">
    <span><?=$this->lang->line('site_social_text')?></span>
    <?php foreach ($this->config->item('site_social') as $row) : ?>
      <a href="<?=$row['link']?>" title="<?=$this->lang->line('site_social_title_'.$row['name'])?>" target="_blank">
        <i class="fab <?=$row['faIcon']?> fa-2x"></i>
      </a>
    <?php endforeach; ?>
  </div>
</div>