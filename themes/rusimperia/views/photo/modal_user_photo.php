<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<form enctype="multipart/form-data">
    <div class="form-group">
            <label for="album_id"><?=$this->lang->line('modal_label_photo_album_select')?></label>
            <select id="album_id" name="album_id" class="form-control chosen-select">
                <?php foreach($albums as $a) : ?>
                    <option value="<?=$a->id;?>"><?=$a->name;?></option>
                <?php endforeach;?>
            </select>
        </div>
        <div class="form-group">
            <label for="photo"><?=$this->lang->line('modal_label_photo_select')?></label>
            <input type="file" id="photo" name="photo" class="form-control" />
        </div>
        <div class="form-group">
            <label for="descr"><?=$this->lang->line('modal_label_photo_descr')?></label>
            <textarea class="form-control" id="descr" name="description"></textarea>
        </div>
    </div>
</form>
<div class="modal-footer">
    <button type="button" class="btn btn-primary"><?=$this->lang->line('modal_label_photo_save')?></button>
</div>