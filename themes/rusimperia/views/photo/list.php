<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @var int $countAlbums
 * @var array $albums
 * @var int $countPhotos
 * @var array $photos
 */
?>
<div class="photo-grid">
    <div id="albums" class="content-box albums-list">
        <div class="content-header">
            <h3>Альбомы <?=$countAlbums?></h3>
            <?php if($this->user->id === $this->uri->segment(3)) : ?>
            <button type="button" class="btn" onclick="openPhotoModal('user_album')">Создать альбом</button>
            <?php endif; ?>
        </div>

        <?php foreach($albums as $a) : ?>
            <div class="album">
                <img class="album-cover" src="/media/photo/cover/<?=$a->cover?>" />
                <a class="album-title" href="/photo/user/<?=$this->user->id?>/album/<?=$a->id; ?>"><?=$a->name;?></a>
                <p class="album-description"><?=$a->description;?></p>
            </div>
        <?php endforeach;
        if ($countAlbums > count($albums)) : ?>
        <span class="albums-more" onclick="showAllAlbums(this, '<?=$type?>', <?=$id?>)">
            Показать все <?=$countAlbums.' '
            .decl_a_num($countAlbums, '%альбом%альбома%альбомов')?></span>
        <?php endif; ?>
    </div>
    <div id="photos" class="content-box photos-list">
        <div class="content-header">
            <h3>Фотографии <?=$countPhotos?></h3>
            <?php if($this->user->id === $this->uri->segment(3)) : ?>
                <button type="button" class="btn" onclick="openPhotoModal('user_photo')">Добавить фото</button>
            <?php endif; ?>
        </div>


        <?php foreach($photos as $p) : ?>
            <div class="photo-thumb">
                <img class="thumb-img" src="/media/photo/cover/<?=$p->id;?>" />
                <span class="thumb-description"><?=$p->description;?></span>
            </div>
        <?php endforeach; ?>
    </div>
</div>