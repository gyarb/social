<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<form  method="POST">
    <div class="form-group">
        <label for="name"><?=$this->lang->line('modal_label_album_title')?></label>
        <input type="text" id="name" name="name" class="form-control" required/>
        <span class="text-danger" id="name_err" ></span>
    </div>
    <div class="form-group">
        <label for="descr"><?=$this->lang->line('modal_label_album_descr')?></label>
        <textarea class="form-control" id="descr" name="descr"></textarea>
        <span class="text-danger" id="descr_err" ></span>
    </div>
</form>
<div class="modal-footer">
    <span id="status" class="text-center"></span>
    <button type="button" class="btn btn-primary" onclick="createUserAlbum(this)">
        <?=$this->lang->line('modal_label_album_save')?>
    </button>
</div>