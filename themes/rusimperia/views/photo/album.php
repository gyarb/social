<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div id="photos" class="content-box photo-grid">
    <div>
        <h3>
            <a href="/photo/<?=$owner?>/<?=$owner_id?>">Фотографии</a>
            > альбом "<?=$album->name?>" <?=$album->count?></h3>
        <?php if ($this->user->id === $owner_id) : ?>
        <div class="dd-anchor"><i class="fas fa-ellipsis-v"></i>
            <ul class="dropdown">
                <li>
                    <button type="button" class="btn"
                            onclick="openPhotoModal('user_photo', <?=$album->id?>)">
                        Добавить фото
                    </button>
                </li>
                <li>
                    <button type="button" class="btn"
                            onclick="openPhotoModal('edit_album')">
                        Редактировать альбом
                    </button>
                </li>
            </ul>
        </div>
        <?php endif; ?>
    </div>
    <div><?=$album->description?></div>
    <?php foreach($photos as $p) : ?>
    <div class="photo-preview">
        <img class="" src="/media/photo/cover/<?=$p->id;?>" />
        <span><?=$p->description;?></span>
    </div>
    <?php endforeach; ?>
</div>