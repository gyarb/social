function openPhotoModal(type, id) {
    id = (id)?id:null;
    $.ajax({
        url: "/photo/get_modal",
        type: "POST",
        data: {type: type, id: id},
        dataType: "json",
        success: function (json) {
            if (json.status === "OK") { //ошибок не было
                $('.modal-footer').css({'display': 'none'});
                openModal({
                    'header': json.html.h,
                    'body': json.html.b
                }, false);
            }
        },
        error: function () {
            $('.modal-header').css({'display': 'none'});
            $('.modal-footer').css({'display': 'none'});
            openModal({
                'body': '<span class="text-danger">Что-то пошло не так. Попробуйте позже.</span>',
                'footer': '<s' + 'cript>' +
                    'setTimeout(function(){closeModal()}, 2e3)' +
                    '</s' + 'cript>'
            }, false);
        }
    });
}

function createUserAlbum(e) {
    "use strict";
    var oldBtn;
    var formData = new FormData();
    formData.append("name", $("#name").val());
    formData.append("descr", $("#descr").val());

    $.ajax({
        url: "/photo/create_user_album",
        type: "POST",
        data: formData,
        dataType: "json",
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
            oldBtn = $(e).text();
            $('#name_err').html('');
            $('#descr_err').html('');
            $(e).addClass("btn-primary").html('<i class="fas fa-spinner fa-spin"></i>');
        },

        success: function (json) {
            $(e).removeClass("btn-primary").text(oldBtn);
            if (json.status == "OK") { //ошибок не было
                $('#status').addClass("bg-success")
                    .removeClass("bg-danger bg-info bg-warning")
                    .html(json.message+'<s'+'cript>'+
                        'setTimeout(function(){location.reload()}, 3e3)'+
                        '</s'+'cript>')
                    .css({'margin-bottom':'10px','padding':'10px 0'});

            } else { //ошибки были, показываем их описание
                $('#status').addClass("bg-danger")
                    .removeClass("bg-success bg-info bg-warning")
                    .html(json.message)
                    .css({'margin-bottom':'10px','padding':'10px 0'});

                $('#name_err').html(json.name_err);
                $('#descr_err').html(json.descr_err);
            }
        },

        error: function () {
            $('#status').addClass("bg-warning")
                .removeClass("bg-danger bg-info bg-success")
                .html('<span class="text-danger">Что-то пошло не так. Попробуйте позже.</span>')
                .css({'margin-bottom':'10px','padding':'10px 0'});
        }
    });
}

function showAllAlbums(e, type, id) {
    $.ajax({
        url: "/photo/get_albums",
        type: "POST",
        data: {type: type, id: id},
        dataType: "json",
        success: function (json) {
            if (json.status === "OK") {
                $(e).remove();
                if (json.albums.length > 0) {
                    $.each(json.albums, function(index, data){
                        $("#albums").append(data);
                    });
                }
            }
        },
        error: function () {
            $('.modal-header').css({'display': 'none'});
            $('.modal-footer').css({'display': 'none'});
            openModal({
                'body': '<span class="text-danger">Что-то пошло не так. Попробуйте позже.</span>',
                'footer': '<s' + 'cript>' +
                    'setTimeout(function(){closeModal()}, 2e3)' +
                    '</s' + 'cript>'
            }, false);
        }
    });
}