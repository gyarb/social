<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?= $title; ?>        
    </h1>      
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-info">
          <div class="box-header">
            <h3 class="box-title">Статические страницы</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Название</th>               
                <th>Name</th>               
                <th>Последнее редактирование</th>               
              </tr>
              </thead>
              <tbody>
              <?php foreach($pages as $p) : ?>               
                <tr>                
                  <td><a href="/admin/pages/item/<?php echo $p->id; ?>"><?= $p->title; ?></a></td>
                  <td><?= $p->name; ?></td>
                  <td><?= $p->last_edit; ?></td>
                </tr>              
              <?php endforeach;?>
              </tbody>
              <tfoot>
              <tr>
                <th>Название</th>
                <th>Name</th>
                <th>Последнее редактирование</th>               
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->      

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->  