<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?= $title; ?>        
    </h1>
    Страница а разработке...      
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-xs-12">
        
        <?php if($id == ''): ?>

        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Данные пользователя</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form class="form-horizontal" method="POST" action="/admin/users/add_user">
            <div class="box-body">

              <div class="form-group">
                <label for="first_name" class="col-sm-2 control-label">Имя</label>
                <div class="col-sm-10">
                  <input name="first_name" type="text" class="form-control" id="first_name" placeholder="Имя" >
                </div>
              </div>

              <div class="form-group">
                <label for="last_name" class="col-sm-2 control-label">Фамилия</label>
                <div class="col-sm-10">
                  <input name="last_name" type="text" class="form-control" id="last_name" placeholder="Фамилия" >
                </div>
              </div>          

            </div>
            <!-- /.box-body -->
            <div class="box-footer">              
              <button type="submit" class="btn btn-info pull-right">Добавить</button>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>

        <?php else: ?>

        <div class="box box-success">          
          <div class="col-xs-12">
            
              <h4>Пользователь успешно добавлен!</h4>
              <p><a href="/admin/users/item/<?= $id; ?>">Редактировать пользователя</a></p>
              <p><a href="/admin/users/add">Добавить нового пользователя</a></p>
            
          </div>
          <div class="clearfix"></div>
        </div>

        <?php endif; ?>

      </div>
    </div>
    <!-- /.row -->      

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->  