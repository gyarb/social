<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?= $title; ?>        
    </h1>      
  </section>

  <!-- Main content -->
  <section class="content"> 

    <div class="row">
      <div class="col-xs-12">

      <?php if($result == ''): ?>            

        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Сообщение</h3>
          </div>
          <!-- /.box-header -->

          <!-- form start -->
          <form class="form-horizontal" method="POST" action="/admin/support/delete_message">
            <div class="box-body">
              
              <div class="form-group">
                <label for="date" class="col-sm-2 control-label">Дата</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="date" value="<?= $message->date; ?>" readonly>
                </div>
              </div>

              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Имя</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="name" value="<?= $message->name; ?>" readonly>
                </div>
              </div>

              <div class="form-group">
                <label for="theme" class="col-sm-2 control-label">Тема</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="theme" value="<?= $message->theme; ?>" readonly>
                </div>
              </div>

              <div class="form-group">
                <label for="message" class="col-sm-2 control-label">Сообщение</label>
                <div class="col-sm-10">                  
                  <textarea id="message" class="form-control" readonly><?= $message->message; ?></textarea>
                </div>
              </div>

              <input type="hidden" name="id" value="<?= $message->id; ?>">         

            </div>
            <!-- /.box-body -->
            <div class="box-footer">              
              <button type="submit" class="btn pull-right" name="delete_message">Удалить</button>
            </div>
            <!-- /.box-footer -->
          </form>

        </div>
        <!-- /.box -->

        <?php else: ?>

          <?php if($result == 'ok'): ?>
            <div class="box box-success">
              <div class="box-header with-border">
                <h3 class="box-title">Сообщение удалено!</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <p><a href="/admin/support/all">Перейти к списку сообщений</a></p>
              </div>
            </div>
            <!-- /.box -->
          <?php endif; ?>

          <?php if($result == 'error'): ?>
            <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title">Произошла ошибка при удалении сообщения!</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <p><a href="/admin/support/all">Перейти к списку сообщений</a></p>
              </div>
            </div>
            <!-- /.box -->
          <?php endif; ?>

        <?php endif; ?>

      </div>
    </div>
    <!-- /.row -->      

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper --> 