<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?= $title; ?>        
    </h1>      
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-xs-12">

        <?php if($result == 'ok'): ?>

          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Данные успешно обновлены!</h3>
            </div>
            <div class="box-body">
              <p><a href="/admin/support/themes">Перейти к списку тем сообщений</a></p>
            </div>
          </div>

        <?php endif; ?>

        <?php if($result == 'error'): ?>

          <div class="box box-danger">            
            <div class="box-header with-border">
              <h3 class="box-title">При обновлении данных произошла ошибка!</h3>
            </div>
            <div class="box-body">
              <p><a href="/admin/support/themes">Перейти к списку тем сообщений</a></p>
            </div>
          </div>

        <?php endif; ?>

        <?php if ($result == 'delete_ok' || $result == 'delete_error'): ?>

          <?php if ($result == 'delete_ok'): ?>
            <div class="box box-success">
              <div class="box-header with-border">
                <h3 class="box-title">Тема сообщений удалена!</h3>
              </div>
              <div class="box-body">
                <p><a href="/admin/support/themes">Перейти к списку тем сообщений</a></p>
              </div>
            </div>
          <?php endif; ?>

          <?php if ($result == 'delete_error'): ?>
            <div class="box box-danger">            
              <div class="box-header with-border">
                <h3 class="box-title">При удалении данных произошла ошибка!</h3>
              </div>
              <div class="box-body">
                <p><a href="/admin/support/themes">Перейти к списку тем сообщений</a></p>
              </div>
            </div>
          <?php endif; ?>

        <?php else: ?>

        <?php if ($result == 'delete_no'): ?>
          <div class="box box-danger">            
            <div class="box-header with-border">
              <h3 class="box-title">Удалить нельзя! C данной темой существуют сообщения!</h3>
            </div>
            <div class="box-body">
              <p><a href="/admin/support/themes">Перейти к списку тем сообщений</a></p>
              <p><a href="/admin/support/all">Перейти к списку сообщений</a></p>
            </div>
          </div>
        <?php endif; ?>  

        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Тема сообщений</h3>
          </div>
          <!-- /.box-header -->
          
          <!-- form start -->
          <form class="form-horizontal" method="POST" action="/admin/support/themes_item/<?= $theme->theme_id; ?>">
            <div class="box-body">

              <div class="bg-danger text-center">
                <?php echo validation_errors(); ?>
              </div>

              <input name="theme_id" type="hidden" value="<?= $theme->theme_id; ?>">

              <div class="form-group">
                <label for="theme" class="col-sm-2 control-label">Тема</label>
                <div class="col-sm-10">
                  <input name="theme" type="text" class="form-control" id="theme" value="<?= $theme->theme; ?>"  placeholder="Тема сообщения" required>
                </div>
              </div>

              <div class="form-group">
                <label for="active" class="col-sm-2 control-label">Видимость</label>
                <div class="col-sm-10">
                  <select name="active" class="form-control" id="active">
                    <option value="1" <?= set_select('active', '1', ($theme->active === '1')?true:false);?>>Да</option>
                    <option value="0" <?= set_select('active', '0', ($theme->active === '0')?true:false);?>>Нет</option>                    
                  </select>
                </div>
              </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">              
              <button type="submit" class="btn btn-info pull-right" name="edit_theme">Обновить</button>
              <button type="submit" class="btn pull-left" name="delete_theme">Удалить</button>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
        <!-- /.box -->

        <?php endif; ?>

      </div>
    </div>
    <!-- /.row -->      

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper --> 