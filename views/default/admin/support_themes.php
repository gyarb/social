<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?= $title; ?>        
    </h1>      
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Темы сообщений</h3>
          </div>
          <!-- /.box-header -->     
          <div class="box-body">
            <table class="table table-bordered table-striped">
              <thead>
              <tr>                
                <th>Тема</th>                              
                <th>Видимость</th>                              
              </tr>
              </thead>
              <tbody>
              <?php foreach($themes as $item) : ?>               
              <tr>                
                <td><a href="/admin/support/themes_item/<?= $item->theme_id ?>"><?= $item->theme; ?></a></td>
                <td><?php if($item->active) {echo 'да';} else {echo 'нет';} ?></td>
              </tr>
              <?php endforeach;?>
              </tbody>
              <tfoot>
              <tr>                
                <th>Тема</th>
                <th>Видимость</th>                
              </tr>
              </tfoot>
            </table>            
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <?php if($result != ''): ?>
          <?php if($result == 'ok'): ?>
            <div class="box box-success">
              <div class="box-header with-border">
                <h3 class="box-title">Тема успешно добавлена!</h3>
              </div>
              <!-- /.box-header -->
            </div>
            <!-- /.box -->
          <?php endif; ?>
          <?php if($result == 'error'): ?>
            <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title">Произошла ошибка при добавлении темы!</h3>
              </div>
              <!-- /.box-header -->
            </div>
            <!-- /.box -->
          <?php endif; ?>
        <?php endif; ?>

        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Добавление темы сообщений</h3>
          </div>
          <!-- /.box-header -->

          <!-- form start -->
          <form class="form-horizontal" method="POST" action="/admin/support/themes">
            <div class="box-body">
              <div class="bg-danger text-center">
                <?php echo validation_errors(); ?>
              </div>
              <div class="form-group">
                <label for="theme" class="col-sm-2 control-label">Тема</label>
                <div class="col-sm-10">
                  <input name="theme" type="text" class="form-control" id="theme" placeholder="Тема сообщения" required>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">              
              <button type="submit" class="btn btn-info pull-right" name="add_theme">Добавить</button>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper --> 