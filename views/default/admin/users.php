<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?= $title; ?>        
    </h1>      
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-info">
          <div class="box-header">
            <h3 class="box-title">Зарегистрированные пользователи</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>ID</th>
                <th>Имя</th>
                <th>Ip адрес</th>
                <th>Страна</th>
                <th>Город</th>
                <th>Email</th>
                <th>Посл.активность</th>
              </tr>
              </thead>
              <tbody>
              <?php foreach($users as $u) : ?>               
              <tr>
                <td><?php echo $u->id; ?></td>
                <td><a href="/admin/users/item/<?php echo $u->id; ?>"><?php echo $u->first_name.' '.$u->last_name; ?></a></td>
                <td><?php echo $u->ip_address; ?></td>
                <td><?php //echo $u->country; ?></td>
                <td><?php //echo $u->city; ?></td>
                <td><?php echo $u->email; ?></td>
                <td><?php echo $u->last_login; ?></td>
              </tr>
              <?php endforeach;?>
              </tbody>
              <tfoot>
              <tr>
                <th>ID</th>
                <th>Имя</th>
                <th>Ip адрес</th>
                <th>Страна</th>
                <th>Город</th>
                <th>Email</th>
                <th>Посл.активность</th>
              </tr>
              </tfoot>
            </table>
            <div class="box-footer clearfix">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->      

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->  