<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?= $title; ?>        
    </h1>      
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- <pre>
      <?php print_r($avatar); ?>
    </pre> -->

    <div class="row">
      <div class="col-xs-12">

        <?php if($result == 'ok'): ?>

          <div class="box box-success">
            <div class="col-xs-12">              
                <h4>Данные успешно обновлены!</h4>
            </div>
            <div class="clearfix"></div>
          </div>

        <?php endif; ?>

        <?php if($result == 'error'): ?>

          <div class="box box-danger">
            <div class="col-xs-12">              
                <h4>При обновлении данных произошла ошибка!</h4>
            </div>
            <div class="clearfix"></div>
          </div>

        <?php endif; ?>     

        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Данные пользователя</h3>
          </div>
          <!-- /.box-header -->
          <br>
          <div class="col-sm-2 text-right">            
            <img src="/media/user/avatar/<?= $avatar; ?>" style="width: 90px; height: auto;">
          </div>
          <div class="col-sm-10">
            <p>Id: <?= $user->user_id; ?></p>
            <p>Ip адрес: <?= $user->ip_address; ?></p>
            <p>Никнейм: <?= $user->username; ?></p>
          </div>
          <div class="clearfix"></div>

          <!-- form start -->
          <form class="form-horizontal" method="POST" action="/admin/users/edit_user">
            <div class="box-body">

              <input name="user_id" type="hidden" value="<?= $user->user_id; ?>">
              <input name="active_old" type="hidden" value="<?= $user->active; ?>">              

              <div class="form-group">
                <label for="first_name" class="col-sm-2 control-label">Имя</label>
                <div class="col-sm-10">
                  <input name="first_name" type="text" class="form-control" id="first_name" placeholder="Имя" value="<?= $user->first_name; ?>" required>
                </div>
              </div>

              <div class="form-group">
                <label for="last_name" class="col-sm-2 control-label">Фамилия</label>
                <div class="col-sm-10">
                  <input name="last_name" type="text" class="form-control" id="last_name" placeholder="Фамилия" value="<?= $user->last_name; ?>" required>
                </div>
              </div>

              <div class="form-group">
                <label for="gender" class="col-sm-2 control-label">Пол</label>
                <div class="col-sm-10">
                  <select name="gender" class="form-control" id="gender">
                    <option value="ns" <?= set_select('gender', 'ns', ($user->gender === 'ns')?true:false);?>>Не указан</option>
                    <option value="male" <?= set_select('gender', 'male', ($user->gender === 'male')?true:false);?>>М</option>
                    <option value="female" <?= set_select('gender', 'female', ($user->gender === 'female')?true:false);?>>Ж</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="active" class="col-sm-2 control-label">Блокирование</label>
                <div class="col-sm-10">
                  <select name="active" class="form-control" id="active">
                    <option value="1" <?= set_select('active', '1', ($user->active === '1')?true:false);?>>Активен</option>
                    <option value="0" <?= set_select('active', '0', ($user->active === '0')?true:false);?>>Заблокирован</option>                    
                  </select>
                  <p class="input-description bg-warning">Внимание! При изменении данного поля на "Заблокирован" будет сброшена статистика пользователя.</p>
                </div>
              </div>          

            </div>
            <!-- /.box-body -->
            <div class="box-footer">              
              <button type="submit" class="btn btn-info pull-right">Обновить</button>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->      

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper --> 