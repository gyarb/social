<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?= $title; ?>        
    </h1>      
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-info">
          <div class="box-header">
            <h3 class="box-title">Сообщения от пользователей</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Имя</th>
                <th>Тема</th>
                <th>Дата, время</th>                
              </tr>
              </thead>
              <tbody>
              <?php foreach($messages as $m) : ?>               
              <tr>
                <td><?= $m->name ?></td>
                <td><a href="/admin/support/item/<?= $m->id ?>"><?= $m->theme ?></a></td>
                <td><?= $m->date ?></td>                                
              </tr>
              <?php endforeach;?>
              </tbody>
              <tfoot>
              <tr>
                <th>Имя</th>
                <th>Тема</th>
                <th>Дата, время</th>
              </tr>
              </tfoot>
            </table>
            <div class="box-footer clearfix">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->      

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->  