<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Админ-панель | rusimperia.ru</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/assets/admin/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="/assets/admin/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/assets/admin/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="/assets/admin/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="/assets/admin/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="/assets/admin/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="/assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="/assets/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="/assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <link rel="stylesheet" href="/assets/admin/style.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>R</b>I</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Rus</b>Imperia</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->
              <span class="hidden-xs"><?= $admin->first_name; ?> <?= $admin->last_name; ?></span>
            </a>
            <ul class="dropdown-menu">              
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-6 text-center">
                    <a href="/" target="_blank">На сайт</a>
                  </div>
                  <div class="col-xs-6 text-center">
                    <a href="/auth/logout">Выход</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>              
            </ul>
          </li>

          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">     
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">        
        <li>
          <a href="/admin/index"><i class="fa fa-dashboard"></i> <span>Главная</span></a>
        </li>

        <li class="treeview" >
          <a href="#">
            <i class="fa fa-user"></i> <span>Пользователи</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="/admin/users/all"><i class="fa fa-circle-o"></i> Все пользователи</a>
            </li>
            <li>
              <a href="/admin/users/add"><i class="fa fa-circle-o"></i> Добавить пользователя</a>
            </li>            
          </ul>
        </li>

        <li class="treeview" >
          <a href="#">
            <i class="fa fa-file"></i> <span>Страницы</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="/admin/pages/all"><i class="fa fa-circle-o"></i> Все страницы</a>
            </li>
            <li>
              <a href="/admin/pages/add"><i class="fa fa-circle-o"></i> Добавить страницу</a>
            </li>            
          </ul>
        </li>

        <li class="treeview" >
          <a href="#">
            <i class="fa fa-smile-o"></i> <span>Поддержка</span>
            <?php if($support_count): ?>
            <span>
              <small class="label bg-yellow"><?= $support_count; ?></small>
            </span>
            <?php endif; ?>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="/admin/support/all"><i class="fa fa-circle-o"></i> Сообщения
              <?php if($support_count): ?>
              <span>
                <small class="label">(<?= $support_count; ?>)</small>
              </span>
              <?php endif; ?>
              </a>
            </li>
            <li>
              <a href="/admin/support/themes"><i class="fa fa-circle-o"></i> Темы сообщений</a>
            </li>            
          </ul>
        </li>        
        <!-- <li><a href="/admin/banners"><i class="fa fa-image"></i> <span>Баннеры</span></a></li> -->
        <!-- <li>
          <a href="/admin/abuse">
            <i class="fa fa-smile-o"></i> <span>Жалобы</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-yellow">2</small>
            </span>
          </a>
        </li>  -->       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>