<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?= $title; ?>        
    </h1>      
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-xs-12">

        <?php if($result != ''): ?>

          <?php if($result == 'ok'): ?>

            <div class="box box-success">
              <div class="box-header with-border">
                <h3 class="box-title">Страница успешно добавлена!</h3>
              </div>
              <div class="box-body">
                <p><a href="/admin/pages/all">Перейти к списку страниц</a></p>
                <p><a href="/admin/pages/add">Добавить новую страницу</a></p>
              </div>
            </div>

          <?php endif; ?>

          <?php if($result == 'error'): ?>

            <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title">При добавлении страницы произошла ошибка!</h3>
              </div>
              <div class="box-body">
                <p><a href="/admin/pages/all">Перейти к списку страниц</a></p>
                <p><a href="/admin/pages/add">Добавить новую страницу</a></p>
              </div>
            </div>

          <?php endif; ?>

        <?php else: ?>        

        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Статическая страница</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form class="form-horizontal" method="POST" action="/admin/pages/insert_page">
            <div class="box-body">

              <input name="page_id" type="hidden">

              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10">
                  <input name="title" type="text" class="form-control" id="title" required>
                </div>
              </div>
              
              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                  <input name="name" type="text" class="form-control" id="name" required>
                  <p class="input-description">Используйте только латинские буквы.</p>
                </div>
              </div>

              <div class="form-group">
                <label for="descr" class="col-sm-2 control-label">Описание</label>
                <div class="col-sm-10">
                  <input name="descr" type="text" class="form-control" id="descr" >
                </div>
              </div>

              <div class="form-group">
                <label for="keywords" class="col-sm-2 control-label">Ключевые слова</label>
                <div class="col-sm-10">
                  <input name="keywords" type="text" class="form-control" id="keywords" >
                </div>
              </div>

              <div class="form-group">
                <label for="head" class="col-sm-2 control-label">Заголовок</label>
                <div class="col-sm-10">
                  <textarea id="head" name="head" rows="10" cols="80" required>                    
                  </textarea>
                </div>
              </div>

              <div class="form-group">
                <label for="content" class="col-sm-2 control-label">Содержание</label>
                <div class="col-sm-10">
                  <textarea id="content" name="content" rows="10" cols="80" required>                    
                  </textarea>
                </div>
              </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">              
              <button type="submit" class="btn btn-info pull-right">Добавить</button>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
        <!-- /.box -->

        <?php endif; ?>

      </div>
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper --> 